<?php

namespace App\Http\Controllers;

use App\Http\Resources\DataPenjualanResource;
use App\Models\DataPenjualan;
use App\Models\DataProduksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class DataPenjualanController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $penjualan = DataPenjualan::all()->sortByDesc('created_at');
    return response(DataPenjualanResource::collection($penjualan), 200);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $penjualan = null;

    DB::transaction(function () use (&$penjualan, $request) {
      $penjualan = DataPenjualan::create([
        'id_produksi' => $request->input('id_produksi'),
        'id_pelanggan' => $request->input('id_pelanggan'),
        'jumlah' => $request->input('jumlah'),
      ]);

      $dataProduksi = DataProduksi::find($request->input('id_produksi'));
      $dataProduksi->stok = $dataProduksi->stok - $request->input('jumlah');
      $dataProduksi->terjual = $dataProduksi->terjual + $request->input('jumlah');
      $dataProduksi->save();
    });

    return response($penjualan, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\DataPenjualan  $dataPenjualan
   *    * @return \Illuminate\Http\Response
   */
  public function show(DataPenjualan $penjualan)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\DataPenjaualan  $dataPenjualan
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, DataPenjualan $penjualan)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\DataPenjualan  $dataPenjualan
   * @return \Illuminate\Http\Response
   */
  public function destroy(DataPenjualan $penjualan)
  {
    DB::transaction(function () use ($penjualan) {
      $dataProduksi = DataProduksi::find($penjualan->id_produksi);
      $dataProduksi->stok = $dataProduksi->stok + $penjualan->jumlah;
      $dataProduksi->terjual = $dataProduksi->terjual - $penjualan->jumlah;
      $dataProduksi->save();

      $penjualan->delete();
    });

    return response('', 204);
  }
}
