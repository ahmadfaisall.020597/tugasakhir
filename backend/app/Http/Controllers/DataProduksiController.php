<?php

namespace App\Http\Controllers;

use App\Http\Resources\DataProduksiResource;
use App\Models\DataProduksi;
use Illuminate\Http\Request;

class DataProduksiController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $produksi = DataProduksi::all()->sortByDesc('created_at');
    return response(DataProduksiResource::collection($produksi), 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $produksi = DataProduksi::create([
      'id_produksi' => $request->input('id_produksi'),
      'id_produk' => $request->input('id_produk'),
      'jumlah_produksi' => $request->input('jumlah'),
      'terjual' => 0,
      'stok' => $request->input('jumlah')
    ]);

    return response($produksi, 201);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Models\DataProduksi  $dataProduksi
   * @return \Illuminate\Http\Response
   */
  public function show(DataProduksi $produksi)
  {
    return response($produksi, 200);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\DataProduksi  $dataProduksi
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, DataProduksi $produksi)
  {
    $produksi->jumlah_produksi = $request->input('jumlah_produksi') ?? $produksi->jumlah_produksi;
    $produksi->terjual = $request->input('terjual') ?? $produksi->terjual;
    $produksi->save();

    return response($produksi, 200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\DataProduksi  $dataProduksi
   * @return \Illuminate\Http\Response
   */
  public function destroy(DataProduksi $produksi)
  {
    $produksi->delete();

    return response('', 204);
  }
}
