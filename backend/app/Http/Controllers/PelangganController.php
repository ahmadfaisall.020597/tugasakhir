<?php

namespace App\Http\Controllers;

use App\Models\Pelanggan;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $pelanggan = Pelanggan::all();

    return response($pelanggan, 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $pelanggan = Pelanggan::create([
      'nama' => $request->input('nama'),
      'no_telepon' => $request->input('no_telepon'),
    ]);

    return response($pelanggan, 201);
  }


  /**
   * Display the specified resource.
   *
   * @param  \App\Models\Pelanggan  $pelanggan
   * @return \Illuminate\Http\Response
   */
  public function show(Pelanggan $pelanggan)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Models\Pelanggan  $pelanggan
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Pelanggan $pelanggan)
  {
    $pelanggan->nama = $request->input('nama');
    $pelanggan->no_telepon = $request->input('no_telepon');
    $pelanggan->save();

    return response($pelanggan, 200);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Models\Pelanggan  $pelanggan
   * @return \Illuminate\Http\Response
   */
  public function destroy(Pelanggan $pelanggan)
  {
    $pelanggan->delete();

    return response('', 204);
  }
}
