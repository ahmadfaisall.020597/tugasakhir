<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
  function index()
  {
    $users = User::all();

    return response($users->toJson(), 200);
  }

  function register(Request $req)
  {
    $user = new User;
    $user->name = $req->input('name');
    $user->email = $req->input('email');
    $user->is_admin = false;
    $user->password = Hash::make($req->input('password'));
    $user->save();
    return $user;
  }

  function update(Request $request, $id)
  {
    $user = User::find($id);
    $user->name = $request->input('name');
    $user->email = $request->input('email');

    if ($request->input('password') != null) {
      $user->password = Hash::make($request->input('password'));
    }

    $user->save();

    return response($user, 200);
  }

  function login(Request $req)
  {
    $user = User::where('email', $req->email)->first();
    if (!$user || !Hash::check($req->password, $user->password)) {
      return response("Sorry, Email or Password doesn't match", 401);
    }
    return $user;
  }

  function destroy($id)
  {
    $user = User::find($id);
    $user->delete();

    return response('', 204);
  }
}
