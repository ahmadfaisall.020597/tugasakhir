<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InputPenjualan extends Model
{
    use HasFactory;
    protected $table = 'inputpenjualan';
    protected $fillable = [
        'namaProduk',
        'pelanggan',
        'jumlah',
    ];
}
