<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        h1 {
            text-align: center;
        }
        .table1 {
            font-family: sans-serif;
            color: #444;
            border-collapse: collapse;
            width: 100%;
            border: 1px solid #f2f5f7;
        }

        .table1 tr th {
            background: #35A9DB;
            color: #fff;
            font-weight: normal;
        }

        .table1,
        th,
        td {
            padding: 8px 20px;
            text-align: center;
        }

        .table1 tr:hover {
            background-color: #f5f5f5;
        }

        .table1 tr:nth-child(even) {
            background-color: #f2f2f2;
        }
    </style>

<body>
    <h1>Laporan Penjualan</h1>
    <table class="table1">
        <thead>
            <tr>
                <th>ID Produksi</th>
                <th>Nama Produk</th>
                <th>Waktu Produksi</th>
                <th>Pelanggan</th>
                <th>Terjual</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($penjualan as $element)
                <tr>
                    <td>{{ $element->dataProduksi->id_produksi }}</td>
                    <td>{{ $element->dataProduksi->produk->nama }}</td>
                    <td>{{ $element->dataProduksi->created_at }}</td>
                    <td>{{ $element->pelanggan->nama }}</td>
                    <td>{{ $element->jumlah }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
