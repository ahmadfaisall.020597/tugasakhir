<?php

use App\Http\Controllers\DataProduksiController;
use App\Http\Controllers\DataPenjualanController;
use App\Http\Controllers\PelangganController;
use App\Http\Controllers\ProdukController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Resources\DataPenjualanResource;
use App\Http\Resources\DataProduksiResource;
use App\Models\DataProduksi;
use App\Models\DataPenjualan;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  return $request->user();
});

Route::controller(UserController::class)->group(function () {
  Route::get('users/index', 'index');
  Route::put('users/{id}', 'update');
  Route::delete('users/{id}', 'destroy');
  Route::post('register', 'register');
  Route::post('login', 'login');
});

Route::get('product-id', function () {
  $id = DataProduksi::orderByDesc('id')
    ->first()
    ?->id;

  return $id ?? 0;
});

Route::get('data-stok', function () {
  $data_stok = DataProduksi::where('stok', '>', 0)
    ->get();

  return response(DataProduksiResource::collection($data_stok), 200);
});

Route::get('dashboard', function() {
$dashboard = DataPenjualan::get();
return response(DataPenjualanResource::collection($dashboard), 200);
});
Route::get('laporan', function() {
  $laporan = DataPenjualan::get();
  return response(DataPenjualanResource::collection($laporan), 200);
  });

Route::controller(ProdukController::class)->group(function () {
  Route::get('products', 'index');
  Route::post('products', 'store');
  Route::put('products/{produk}', 'update');
  Route::patch('products/{produk}', 'update');
  Route::delete('products/{produk}', 'destroy');
});

Route::controller(DataProduksiController::class)->group(function () {
  Route::get('produksi', 'index');
  Route::post('produksi', 'store');
  Route::get('produksi/{produksi}', 'show');
  Route::put('produksi/{produksi}', 'update');
  Route::patch('produksi/{produksi}', 'update');
  Route::delete('produksi/{produksi}', 'destroy');
});

Route::controller(DataPenjualanController::class)->group(function () {
  Route::get('penjualan', 'index');
  Route::post('penjualan', 'store');
  Route::delete('penjualan/{penjualan}', 'destroy');
});

Route::controller(PelangganController::class)->group(function () {
  Route::get('pelanggan', 'index');
  Route::post('pelanggan', 'store');
  Route::put('pelanggan/{pelanggan}', 'update');
  Route::patch('pelanggan/{pelanggan}', 'update');
  Route::delete('pelanggan/{pelanggan}', 'destroy');
});
