import { useMemo, useState, useEffect } from "react";
import axios from "axios";
import CekDataStokPage from "../Fitur/CekDataStokPage";
import DataPelangganPage from "../Fitur/DataPelangganPage";
import InputDataPage from "../Fitur/InputDataPage";
import InputDataPenjualanPage from "../Fitur/InputDataPenjualanPage";
import DataPenjualanPage from "../Fitur/DataPenjualanPage";
import LaporanPenjualan from "../Fitur/LaporanPenjualan";
import PengaturanUserPage from "../Fitur/PengaturanUserPage";
import QualityCheckPage from "../Fitur/QualityCheckPage";
import SideNavComponent from "./Komponen/SideNavComponent";
import InputProdukPage from "../Fitur/InputProdukPage";
import DataProdukPage from "../Fitur/DataProdukPage";


function ThisPage() {
  const [dashboard, setDashboard] = useState([]);

  useEffect(() => {
    axios
      .get('/api/dashboard')
      .then((value) => setDashboard(value.data));
  }, []);

  const dashboardElement = useMemo(() => dashboard.map((element) => (
    <tr>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.id_produksi}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.data_produksi.produk}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.data_produksi.waktu_produksi}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.waktu_penjualan}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.pelanggan.nama}</td>
      <td className="border-collapse border-y border-y-gray-300 text-left p-2">{element.jumlah}</td>
    </tr>
  )), [dashboard]);

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100">
      <h1 className="font-roboto text-4xl text-black mb-4">Dashboard</h1>
      <div className="flex flex-row w-full m-4 bg-white p-4">
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">ID Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Nama Produk</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Waktu Produksi</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Waktu Penjualan</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Pelanggan</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Jumlah</th>
            </tr>
          </thead>
          <tbody>
            {dashboardElement}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default function DashboardPage(props) {
  const { slug } = props;

  const component = useMemo(() => {
    switch (slug) {
      case 'input-produk':
        return <InputProdukPage />
      case 'data-produk':
        return <DataProdukPage />
      case 'input-data':
        return <InputDataPage />
      case 'quality-check':
        return <QualityCheckPage />
      case 'input-data-penjualan':
        return <InputDataPenjualanPage />
      case 'data-penjualan':
        return <DataPenjualanPage />
      case 'cek-data-stok':
        return <CekDataStokPage />
      case 'data-pelanggan':
        return <DataPelangganPage />
      case 'laporan-penjualan':
        return <LaporanPenjualan />
      case 'pengaturan-user':
        return <PengaturanUserPage />
      default:
        return <ThisPage />
    }
  }, [slug]);

  return (
    <div className="w-screen h-screen flex flex-row">
      <div className="w-[20%] h-full bg-blue-900 overflow-y-scroll">
        <SideNavComponent />
      </div>
      <div className="w-[80%] h-full">
        {component}
      </div>
    </div>
  );
}