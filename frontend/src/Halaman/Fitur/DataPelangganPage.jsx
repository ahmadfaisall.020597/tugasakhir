import { Button, FormControl, FormLabel, Input, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, useDisclosure, useToast } from "@chakra-ui/react";
import { useEffect } from "react";
import { useState } from "react";
import axios from 'axios';
import { useMemo } from "react";

export default function DataPelangganPage() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();
  const [dataPelanggan, setDataPelanggan] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const [tempId, setTempId] = useState(-1);

  const clearForm = () => {
    document.getElementById('nama').value;
    document.getElementById('no_telepon').value;
  }

  const fetchDataPelanggan = () => axios
    .get('/api/pelanggan')
    .then((value) => setDataPelanggan(value.data));

  const submitDataPelanggan = () => axios
    .post('/api/pelanggan', {
      nama: document.getElementById('nama').value,
      no_telepon: document.getElementById('no_telepon').value,
    })
    .then((value) => {
      if (value.status === 201) {
        fetchDataPelanggan();
        toast({
          title: 'Berhasil menambahkan pelanggan',
          position: 'top',
          duration: 2000,
          status: 'success'
        });
        onClose();
        clearForm();
      }
    });

  const updateDataPelanggan = () => axios
    .put('/api/pelanggan/' + tempId, {
      nama: document.getElementById('nama').value,
      no_telepon: document.getElementById('no_telepon').value,
    })
    .then((value) => {
      if (value.status === 200) {
        fetchDataPelanggan();
        toast({
          title: 'Berhasil mengupdate data pelanggan',
          position: 'top',
          duration: 2000,
          status: 'success'
        });
        onClose();
        clearForm();
      }
    });


  useEffect(() => { fetchDataPelanggan() }, []);

  const pelangganElement = useMemo(() => dataPelanggan.map((element, index) => {
    const onDelete = () => axios
      .delete('api/pelanggan/' + element.id)
      .then(() => {
        toast({
          title: 'Berhasil Menghapus Produk',
          duration: 2000,
          status: 'success',
          position: 'top'
        });
        fetchDataPelanggan()
      });

    const onEdit = () => {
      setIsEdit(true);
      setTempId(element.id);
      onOpen();
    }

    return (
      <tr>
        <td className="border-collapse border-y border-y-gray-300 text-left">{index + 1}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left">{element.nama}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left">{element.no_telepon}</td>
        <td className="border-collapse border-y border-y-gray-300 text-left flex flex-row items-center">
          <Button margin="0.5rem" colorScheme="blue" onClick={onEdit}>Ganti Nama</Button>
          <Button margin="0.5rem" colorScheme="red" onClick={onDelete}>Hapus</Button>
        </td>
      </tr>
    )
  }), [dataPelanggan]);

  const onTambahPelanggan = () => {
    setIsEdit(false);
    onOpen();
  }

  return (
    <div className="w-full h-full p-4 font-roboto bg-gray-100">
      <h1 className="font-roboto text-4xl text-black mb-4">Data Pelanggan</h1>
      <div className="flex flex-row w-full m-4">
        <Button colorScheme='blue' onClick={onTambahPelanggan}>Tambah Data Pelanggan</Button>
      </div>
      <div className="flex flex-row w-full m-4 bg-white p-4">
        <table className="table-auto w-full">
          <thead>
            <tr>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">No</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Nama</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">No Telepon</th>
              <th className="border-collapse border-y border-y-gray-300 text-left p-2">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {pelangganElement}
          </tbody>
        </table>
      </div>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        isCentered
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{isEdit ? 'Update' : 'Tambah'} Data Pelanggan</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <FormControl>
              <FormLabel>Nama</FormLabel>
              <Input id="nama" placeholder='Nama' />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Nomor Telepon</FormLabel>
              <Input id="no_telepon" placeholder='+628 xxxx xxxx' />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button onClick={onClose}>Cancel</Button>
            <Button colorScheme='blue' ml={3} onClick={isEdit ? updateDataPelanggan : submitDataPelanggan}>
              Simpan
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
}