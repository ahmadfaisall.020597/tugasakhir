import { Button, Input } from '@chakra-ui/react'
import {
  FormControl,
  FormLabel,
} from '@chakra-ui/react'
import React, { useState, useEffect } from "react";
import {useNavigate } from "react-router-dom";
import SideNavComponent from '../Dashboard/Komponen/SideNavComponent';

export default function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  useEffect(() => {
    if (localStorage.getItem("user-info")) {
      navigate("/dashboard");
    }
  }, []);

  async function login() {
    console.warn(email, password);
    let item = { email, password };
    const result = await fetch("http://localhost:8000/api/login", {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        "Accept": "aplication/json"
      },
      body: JSON.stringify(item)
    });

    if (result.status === 401) {
      setShowAlert(true);
      return;
    }

    const json = await result.json()
    localStorage.setItem("user-info", JSON.stringify(await json))
    navigate("/dashboard");
  }
  return (
    <>
    <div className="h-screen w-screen flex items-center justify-center bg-gray-800">
      <div className="bg-white flex flex-col p-12 w-[40%]">
        <h1 className='font-roboto text-5xl mb-4'>Welcome back!</h1>
        <p className="text-lg font-poppins text-black">Don't have an account, yet? {' '}
            <span className="text-blue-400 underline font-poppins text-lg cursor-pointer" onClick={() => navigate('/register')}>Sign up</span>
        </p>
        <FormControl className='mb-4'>
          <FormLabel htmlFor='email'>Email address</FormLabel>
          <Input id='email' type="email" onChange={(e) => setEmail(e.target.value)} />
        </FormControl>
        <FormControl className='mb-8'>
          <FormLabel htmlFor='password'>Password</FormLabel>
          <Input id='password' type="password" onChange={(e) => setPassword(e.target.value)} />
        </FormControl>
        <Button onClick={login} variant='outline' bgColor="#B5CDF5" _hover={{ bgColor: '#94B7F2' }}>Login</Button>
      </div>
    </div>
    </>
  );
}