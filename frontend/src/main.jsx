import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import LoginPage from './Halaman/Login/LoginPage'
import RegisterPage from './Halaman/Register/RegisterPage'
import './index.css'
import { ChakraProvider } from '@chakra-ui/react'
import DashboardPage from './Halaman/Dashboard/DashboardPage'
import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:8000';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <ChakraProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/dashboard" element={<DashboardPage />} />
          <Route path="/dashboard/input-produk" element={<DashboardPage slug="input-produk" />} />
          <Route path="/dashboard/data-produk" element={<DashboardPage slug="data-produk" />} />
          <Route path="/dashboard/input-data" element={<DashboardPage slug="input-data" />} />
          <Route path="/dashboard/quality-check" element={<DashboardPage slug="quality-check" />} />
          <Route path="/dashboard/input-data-penjualan" element={<DashboardPage slug="input-data-penjualan" />} />
          <Route path="/dashboard/data-penjualan" element={<DashboardPage slug="data-penjualan" />} />
          <Route path="/dashboard/cek-data-stok" element={<DashboardPage slug="cek-data-stok" />} />
          <Route path="/dashboard/data-pelanggan" element={<DashboardPage slug="data-pelanggan" />} />
          <Route path="/dashboard/laporan-produksi-harian" element={<DashboardPage slug="laporan-produksi-harian" />} />
          <Route path="/dashboard/laporan-produksi-bulanan" element={<DashboardPage slug="laporan-produksi-bulanan" />} />
          <Route path="/dashboard/laporan-produksi-tahunan" element={<DashboardPage slug="laporan-produksi-tahunan" />} />
          <Route path="/dashboard/laporan-penjualan" element={<DashboardPage slug="laporan-penjualan" />} />
          <Route path="/dashboard/pengaturan-user" element={<DashboardPage slug="pengaturan-user" />} />
          <Route path="/dashboard/pengaturan-ganti-password" element={<DashboardPage slug="pengaturan-ganti-password" />} />
        </Routes>
      </BrowserRouter>
    </ChakraProvider>
  </React.StrictMode>
)
